
wizard.module
-------------

This module breaks up CCK forms into a series of steps.

Missing features:

  * back navigation
  * display header and footer

Module should also be more thoroughly tested with a bigger variety of content
types.
